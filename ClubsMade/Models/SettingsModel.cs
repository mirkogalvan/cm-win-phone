﻿using SQLitePCL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClubsMade.Models
{
    class SettingsModel
    {
        private string Name;
        private string Value;
        private int Id;
        private static readonly string IdName = "Id";
        private static readonly string NameName = "Name";
        private static readonly string ValueName = "Value";

        public static SettingsModel GetSettingByName(string name)
        {
            string sql = @"SELECT * FROM Settings WHERE name = '" + name  + "'";

            using (var statement = App.SQLiteConn.Prepare(sql))
            {
                SQLiteResult statementResult = statement.Step();
                if (statementResult == SQLiteResult.ROW)
                {
                    SettingsModel setting = new SettingsModel();
                    setting.SetId((int)statement.GetInteger(SettingsModel.IdName));
                    setting.SetName(statement.GetText(SettingsModel.NameName));
                    setting.SetValue(statement.GetText(SettingsModel.ValueName));
                    return setting;
                }
            }

            return null;
        }

        public static bool InsertIfNotExists(string name, string value)
        {
            // check if exists
            if (GetSettingByName("name") != null)
            {
                return true;
            }

            var db = App.SQLiteConn;
            string sql = @"INSERT INTO Settings (Name, Value) VALUES(?, ?)";
            using (var stmnt = db.Prepare(sql))
            {
                stmnt.Bind(1, name);
                stmnt.Bind(2, value);
                stmnt.Step();
            }

            return true;
        }

        public static bool InsertIfNotExists(int id, string name, string value)
        {
            // check if exists
            if (GetSettingByName("name") != null)
            {
                return false;
            }

            var db = App.SQLiteConn;
            string sql = @"INSERT INTO Settings (Id, Name, Value) VALUES(?, ?, ?)";
            using (var stmnt = db.Prepare(sql))
            {
                stmnt.Bind(1, id);
                stmnt.Bind(2, name);
                stmnt.Bind(3, value);
                stmnt.Step();
            }

            return true;
        }

        public static bool UpdateByName(string name, string value)
        {
            int Id = 0;

            // check if exists
            SettingsModel temp = GetSettingByName(name);
            if ( temp != null)
            {
                Id = temp.GetId();
            }

            if (Id > 0)
            {
                var db = App.SQLiteConn;
                string sql = @"UPDATE Settings set Value = ? where Id = ?";
                using (var stmnt = db.Prepare(sql))
                {
                    stmnt.Bind(1, value);
                    stmnt.Bind(2, Id);
                    stmnt.Step();
                }
            }

            return Id > 0;
        }

        public bool Save()
        {
            bool hasId = false;
            if (Id > 0)
                hasId = true;

            if (Value == null || Name == null)
                return false;

            return hasId ? InsertIfNotExists(Id, Name, Value) : InsertIfNotExists(Name, Value);
        }

        public void SetName(string name)
        {
            this.Name = name;
        }

        public void SetValue(string value)
        {
            this.Value = value;
        }

        public void SetId(int id)
        {
            this.Id = id;
        }

        public string GetName()
        {
            return this.Name;
        }

        public string GetValue()
        {
            return this.Value;
        }

        public int GetId()
        {
            return this.Id;
        }
    }
}
