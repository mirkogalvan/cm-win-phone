﻿using ClubsMade.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;
using Windows.Devices.Geolocation;
using ClubsMade.Types;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Popups;
using ClubsMade.Models;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace ClubsMade
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EventsList : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        public EventListType eventListType;
        private int Radius;

        public EventsList()
        {
            this.InitializeComponent();

            this.eventListType = new EventListType();
            this.eventListType.eventItem = new List<EventType>();
            this.Radius = int.Parse(SettingsModel.GetSettingByName("radius").GetValue());
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
            eventsLoadingProgressRing.IsActive = true;
            this.Radius = int.Parse(SettingsModel.GetSettingByName("radius").GetValue());

            await getEventsCall();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        public async Task getEventsCall()
        {
            string eventMediaEndpoint = "bull/event/responder/filterLocation.json";
            string callParams = "";

            Geolocator geo = new Geolocator();
            geo.DesiredAccuracyInMeters = 50;

            try
            {
                Geoposition position = await geo.GetGeopositionAsync(maximumAge: TimeSpan.FromMinutes(5), timeout: TimeSpan.FromSeconds(10));
                string latitude = position.Coordinate.Point.Position.Latitude.ToString("0.00");
                string longitude = position.Coordinate.Point.Position.Longitude.ToString("0.00");

                callParams = App.API_KEY + "&lat=" + latitude + "&long=" + longitude + "&radius=" + Radius + "&time=" + DateTime.Now;
            }
            catch (UnauthorizedAccessException)
            {
                ContentDialog dlg = new ContentDialog();
                dlg.Title = "Geolocation is not enabled!";
                dlg.IsSecondaryButtonEnabled = false;
                ContentDialogResult result = await dlg.ShowAsync();

                return;
            }

            // build the url
            string apiCall = App.API + eventMediaEndpoint + callParams;

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.TryParseAdd("application/json");

            string response = "";

            try
            {
                response = await client.GetStringAsync(new Uri(apiCall));
            }
            catch (Exception)
            {
                // logout
                var db = App.SQLiteConn;

                using (var statement = db.Prepare("DELETE FROM User"))
                {
                    if (statement.Step() == SQLitePCL.SQLiteResult.DONE)
                    {
                        ContentDialog dlg = new ContentDialog();
                        dlg.Title = "Error";
                        dlg.Content = "Internet connection problem. Please try again!";
                        ContentDialogResult result = await dlg.ShowAsync();

                        Frame.Navigate(typeof(MainPage));
                    }
                }
            }

            JsonObject responseObject = JsonObject.Parse(response);
            if (responseObject.GetNamedString("status").Equals("ok"))
            {
                if (responseObject.GetNamedObject("result") != null)
                {
                    eventsLoadingProgressRing.IsActive = false;
                    var eventsArray = responseObject.GetNamedObject("result").GetNamedArray("list");

                    foreach (var item in eventsArray)
                    {
                        float minPrice = 0;
                        int participants = 0;

                        try
                        {
                            minPrice = float.Parse(item.GetObject().GetNamedString("min_price"));
                        }
                        catch (Exception)
                        {
                            // suppress
                        }

                        try
                        {
                            participants = int.Parse(item.GetObject().GetNamedString("participants"));
                        }
                        catch (Exception)
                        {
                            // suppress
                        }

                        EventType tempEvent = new EventType()
                        {
                            Id = int.Parse(item.GetObject().GetNamedString("event_id")),
                            Name = item.GetObject().GetNamedString("name"),
                            ClubName = item.GetObject().GetNamedString("club_name"),
                            StartTime = DateTime.Parse(item.GetObject().GetNamedString("start")),
                            EventNotes = item.GetObject().GetNamedString("event_notes"),
                            Dresscode = item.GetObject().GetNamedString("dresscode_token"),
                            Music = item.GetObject().GetNamedString("theme_token"),
                            Participants = participants,
                            LowestPrice = minPrice,
                            CoverMedia = new MediaType()
                            {
                                Id = int.Parse(item.GetObject().GetNamedString("event_cover_id")),
                                ThumbPointer = item.GetObject().GetNamedString("event_cover"),
                                OriginalPointer = item.GetObject().GetNamedString("event_cover_original"),
                                UriSource = new Uri(MediaType.CDN_ENDPOINT + item.GetObject().GetNamedString("event_cover_original"), UriKind.Absolute)
                            },
                            Club = new ClubType()
                            {
                                Fullname = item.GetObject().GetNamedString("fullname"),
                                Company = item.GetObject().GetNamedString("company"),
                                Id = int.Parse(item.GetObject().GetNamedString("user")),
                                Location = new LocationType()
                                {
                                    Id = int.Parse(item.GetObject().GetNamedString("location")),
                                    Latitude = float.Parse(item.GetObject().GetNamedString("latitude")),
                                    Longitude = float.Parse(item.GetObject().GetNamedString("longitude")),
                                    Address = item.GetObject().GetNamedString("address")
                                },
                                Notes = item.GetObject().GetNamedString("note"),
                                Distance = float.Parse(item.GetObject().GetNamedString("distance")),
                                ProfileMedia = new MediaType()
                                {
                                    ThumbPointer = item.GetObject().GetNamedString("profile_media"),
                                    OriginalPointer = item.GetObject().GetNamedString("profile_media_original"),
                                    UriSource = new Uri(MediaType.CDN_ENDPOINT + item.GetObject().GetNamedString("profile_media"), UriKind.Absolute)
                                },
                                CoverMedia = new MediaType()
                                {
                                    ThumbPointer = item.GetObject().GetNamedString("cover_media"),
                                    OriginalPointer = item.GetObject().GetNamedString("cover_media_original"),
                                    UriSource = new Uri(MediaType.CDN_ENDPOINT + item.GetObject().GetNamedString("cover_media_original"), UriKind.Absolute)
                                }
                            }
                        };

                        eventListType.eventItem.Add(tempEvent);
                    }

                    eventsLv.ItemsSource = eventListType.eventItem;
                }
            }
        }

        private void openSettings(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SettingsPage));
        }

        private void OpenSingleEvent(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SingleEvent), (EventType)((FrameworkElement)sender).DataContext);
        }
    }
}
