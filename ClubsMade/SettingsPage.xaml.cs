﻿using ClubsMade.Common;
using ClubsMade.Models;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace ClubsMade
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SettingsPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        public SettingsPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
            slider.Value = double.Parse(SettingsModel.GetSettingByName("radius").GetValue());
            sliderValue.Text = slider.Value.ToString("0");
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void doLogout(object sender, RoutedEventArgs e)
        {
            var db = App.SQLiteConn;

            using (var statement = db.Prepare("DELETE FROM User"))
            {
                if (statement.Step() == SQLitePCL.SQLiteResult.DONE)
                {
                    Frame.Navigate(typeof(MainPage));
                }
            }
        }

        private void gotoProfile(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(ProfilePage));
        }

        private void onValueChanged(object sender, RoutedEventArgs e)
        {
            if(slider != null && sliderValue != null)
            {
                sliderValue.Text = slider.Value.ToString("0");
                SettingsModel.UpdateByName("radius", sliderValue.Text);
            }
        }

        private void PP_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(BrowserPage), "pp");
        }

        private void TOS_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(BrowserPage), "tos");
        }

        private async void AreYouAClub_Tapped(object sender, TappedRoutedEventArgs e)
        {
            // Define Recipient
            /*EmailRecipient sendTo = new EmailRecipient()
            {
                Name = "The ClubsMade Team",
                Address = "info@clubsmade.com"
            };

            // Create email object
            EmailMessage mail = new EmailMessage();
            mail.Subject = "Feedback from the Windows Phone app";
            mail.Body = "Your app is amazing...";

            // Add recipients to the mail object
            mail.To.Add(sendTo);

            // Open the share contract with Mail only:
            await EmailManager.ShowComposeNewEmailAsync(mail);*/
        }

        private async void Feedback_Tapped(object sender, TappedRoutedEventArgs e)
        {
            // Define Recipient
            /*EmailRecipient sendTo = new EmailRecipient()
            {
                Name = "The ClubsMade Team",
                Address = "info@clubsmade.com"
            };

            // Create email object
            EmailMessage mail = new EmailMessage();
            mail.Subject = "Feedback from the Windows Phone app";
            mail.Body = "Your app is amazing...";

            // Add recipients to the mail object
            mail.To.Add(sendTo);

            // Open the share contract with Mail only:
            await EmailManager.ShowComposeNewEmailAsync(mail);*/
        }

        private void AboutUs_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(BrowserPage), "aboutus");
        }
    }
}
