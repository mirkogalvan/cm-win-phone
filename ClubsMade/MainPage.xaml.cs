﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace ClubsMade
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        public async Task doLoginCall()
        {
            string eventMediaEndpoint = "user/user/authenticate.json";
            string callParams = App.API_KEY + "&username=" + usernameText.Text + "&password=" + passwordText.Password;

            // build the url
            string apiCall = App.API + eventMediaEndpoint + callParams;

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.TryParseAdd("application/json");

            string response = await client.GetStringAsync(new Uri(apiCall));

            JsonObject responseObject = JsonObject.Parse(response);
            if (responseObject.GetNamedString("status").Equals("ok"))
            {
                if (responseObject.GetNamedString("result") != null)
                {
                    var db = App.SQLiteConn;
                    string sql = @"INSERT INTO User (Id, Username, Password) VALUES(?, ?, ?)";
                    using (var stmnt = db.Prepare(sql))
                    {
                        stmnt.Bind(1, responseObject.GetNamedString("result"));
                        stmnt.Bind(2, usernameText.Text);
                        stmnt.Bind(3, passwordText.Password);
                        stmnt.Step();
                    }

                    loginProgressRing.IsActive = false;
                    Frame.Navigate(typeof(EventsList));
                }
            }
        }

        private async void doLogin(object sender, RoutedEventArgs e)
        {
            loginProgressRing.IsActive = true;
            await doLoginCall();
        }

        public void gotoRegister(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(RegistrationForm));
        }
    }
}
