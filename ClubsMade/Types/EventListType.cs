﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClubsMade.Types
{
    public class EventListType : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public List<EventType> eventItem { get; set; }
    }
}
