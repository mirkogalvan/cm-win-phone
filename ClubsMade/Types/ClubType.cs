﻿using System.ComponentModel;

namespace ClubsMade.Types
{
    public class ClubType : INotifyPropertyChanged
    {
        public LocationType Location { get; set; }
        internal int Id { get; set; }
        internal string Fullname { get; set; }
        internal string Company { get; set; }
        internal string Notes { get; set; }
        internal MediaType ProfileMedia { get; set; }
        internal MediaType CoverMedia { get; set; }
        public float Distance { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}