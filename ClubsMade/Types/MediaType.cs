﻿using System;
using System.ComponentModel;
using Windows.UI.Xaml.Media.Imaging;

namespace ClubsMade.Types
{
    public class MediaType : INotifyPropertyChanged
    {
        public static string CDN_ENDPOINT = "http://cdn.mycm.progress44.com/";

        public int Id { get; set; }
        public string ThumbPointer { get; set; }
        public string OriginalPointer { get; set; }
        public Uri UriSource { get; set; }
        
        public override string ToString()
        {
            if (ThumbPointer == null && OriginalPointer == null)
            {
                return "#";
            }
            else if (ThumbPointer == null && OriginalPointer != null)
            {
                return CDN_ENDPOINT + OriginalPointer;
            }
            else if (ThumbPointer != null && OriginalPointer == null)
            {
                return CDN_ENDPOINT + ThumbPointer;
            }
            else
            {
                return CDN_ENDPOINT + ThumbPointer;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}