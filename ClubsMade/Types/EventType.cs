﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ClubsMade.Types
{
    public class EventType : INotifyPropertyChanged
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ClubName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int Participants { get; set; }
        public string EventNotes { get; set; }
        public string Dresscode { get; set; }
        public string Music { get; set; }
        public ClubType Club { get; set; }
        public MediaType CoverMedia { get; set; }
        public float LowestPrice { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        // artist obj.
    }
}
